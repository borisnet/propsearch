<?php

use Illuminate\Database\Seeder;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $properties = [
            [
                'name'     => "The Victoria",
                'price'    => 374662,
                'beds'     => 4,
                'baths'    => 2,
                'storey'   => 2,
                'garage'   => 2,
            ],
            [
                'name'     => "The Xavier",
                'price'    => 513268,
                'beds'     => 4,
                'baths'    => 2,
                'storey'   => 1,
                'garage'   => 2,
            ],
            [
                'name'     => "The Como",
                'price'    => 454990,
                'beds'     => 4,
                'baths'    => 3,
                'storey'   => 2,
                'garage'   => 3,
            ],
            [
                'name'     => "The Aspen",
                'price'    => 384356,
                'beds'     => 4,
                'baths'    => 2,
                'storey'   => 2,
                'garage'   => 2,
            ],
            [
                'name'     => "The Lucretia",
                'price'    => 572002,
                'beds'     => 4,
                'baths'    => 3,
                'storey'   => 2,
                'garage'   => 2,
            ],
            [
                'name'     => "The Toorak",
                'price'    => 521951,
                'beds'     => 5,
                'baths'    => 2,
                'storey'   => 1,
                'garage'   => 2,
            ],
            [
                'name'     => "The Skyscape",
                'price'    => 263604,
                'beds'     => 3,
                'baths'    => 2,
                'storey'   => 2,
                'garage'   => 2,
            ],
            [
                'name'     => "The Clifton",
                'price'    => 386103,
                'beds'     => 3,
                'baths'    => 2,
                'storey'   => 1,
                'garage'   => 1,
            ],
            [
                'name'     => "The Geneva",
                'price'    => 390600,
                'beds'     => 4,
                'baths'    => 3,
                'storey'   => 2,
                'garage'   => 2
                ,
            ],
        ];
        foreach($properties as $property) {
            DB::table('properties')->insert(
                $property
            );
        }
    }
}
