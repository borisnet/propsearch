require('./bootstrap');

window.Vue = require('vue');

import store from './store/';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);

Vue.component("PropertyList", () => import("./components/PropertyList"));
Vue.component("Filters", () => import("./components/Filters"));
Vue.component("Table", () => import("./components/Table"));

const app = new Vue({
  el: '#app',
  store
});
