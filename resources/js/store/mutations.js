
export default {

    FETCH_PROPERTIES(state, properties) {
        if (!properties.length) {
            state.error = 'No results that match current filters selection. Try the reset button?';
            state.properties = [];
        } else {
            state.error = false;
            state.properties = properties.map(property => {
                property.price = '$' + parseFloat(property.price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                return property;
            });
        }
    },

    UPDATE_FILTER(state, filter) {
        state.filters[filter.name] = filter.value;
    },

    RESET_FILTERS(state) {
        state.filters = {
            name: '',
            beds: '',
            baths: '',
            storey: '',
            garage: '',
            price: [200000,600000],
        };
    },

    START_LOADING(state) {
        state.loading = true;
    },

    STOP_LOADING(state) {
        state.loading = false;
    },

    FETCH_ERROR(state, err) {
        state.error = err && err.response ? err.response.statusText : 'Error';
    },

}
