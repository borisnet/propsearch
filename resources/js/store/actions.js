import axios from 'axios';

export default {
    fetchProps({commit}, filters) {
        commit('START_LOADING');
        let url = '/api/property/list';
        if (filters) {
            const queryString = Object.keys(filters).map(key => key + '=' + filters[key]).join('&');
            url += '?'+queryString;
        }
        axios.get(url)
            .then(res => {
                commit('FETCH_PROPERTIES', res.data.data);
                commit('STOP_LOADING');
            }).catch(err => {
                commit('STOP_LOADING');
                commit('FETCH_ERROR', err);
            });
    },
    updateFilter({commit}, filter) {
        commit('UPDATE_FILTER', filter);
    },
    resetFilters({commit}) {
        commit('RESET_FILTERS');
    },
}
