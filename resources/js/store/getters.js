export default {
    properties: state => {
        return state.properties;
    },
    loading: state => {
        return state.loading;
    },
    filters: state => {
        return state.filters;
    },
    error: state => {
        return state.error;
    },
}
