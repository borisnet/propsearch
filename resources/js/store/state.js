let state = {
    loading: true,
    error: false,
    properties: [],
    filters: {
        name: '',
        beds: '',
        baths: '',
        storey: '',
        garage: '',
        price: [200000,600000],
    }
}
export default state
