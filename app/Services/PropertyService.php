<?php
namespace App\Services;

use App\Repositories\PropertyRepository;

class PropertyService
{
    /**
     * $propertyRepository
     *
     * @var undefined
     */
    private $propertyRepository;

    /**
     * __construct
     *
     * @param PropertyRepository $propertyRepository
     * @return void
     */
    public function __construct(
        PropertyRepository $propertyRepository
    ) {
        $this->propertyRepository = $propertyRepository;
    }

    /**
     * list
     *
     * @param mixed $filters
     * @return void
     */
    public function list($filters)
    {
        return $this->propertyRepository->list($filters);
    }
}
