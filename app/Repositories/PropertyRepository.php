<?php

namespace App\Repositories;

use App\Property;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PropertyRepository
{
    /**
     * $property
     *
     * @var undefined
     */
    private $property;

    /**
     * __construct
     *
     * @param Property $property
     * @return void
     */
    public function __construct(Property $property)
    {
        $this->property = $property;
    }

    /**
     * list
     *
     * @param mixed $filters
     * @return array $properties
     */
    public function list($filters)
    {
        $properties = $this->property;

        if(!empty($filters['name'])) {
            $properties = $properties->where('name', 'like', "%{$filters['name']}%");
        }

        if(!empty($filters['price'])) {
            $prices = explode(',', $filters['price']);
            $properties = $properties->whereBetween('price', [(int)$prices[0], (int)$prices[1]]);
        }

        if(!empty($filters['beds'])) {
            $properties = $properties->where('beds', $filters['beds']);
        }

        if(!empty($filters['baths'])) {
            $properties = $properties->where('baths', $filters['baths']);
        }

        if(!empty($filters['storey'])) {
            $properties = $properties->where('storey', $filters['storey']);
        }

        if(!empty($filters['garage'])) {
            $properties = $properties->where('garage', $filters['garage']);
        }

        return $properties->get();
    }
}
