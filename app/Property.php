<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    /**
     * $fillable
     *
     * @var array
     */
    protected $fillable = ['name', 'price', 'beds', 'baths', 'storey', 'garage'];

    /**
     * $casts
     *
     * @var array
     */
    protected $casts = [
        'price' => 'float',
    ];
}
