<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PropertyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'price'    => $this->price,
            'beds'     => (int) $this->beds,
            'baths'     => (int) $this->baths,
            'storey'     => (int) $this->storey,
            'garage'     => (int) $this->garage,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
