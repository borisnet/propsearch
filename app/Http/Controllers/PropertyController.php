<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\PropertyService;
use App\Http\Resources\PropertyResource;

class PropertyController extends Controller
{
    /**
     * $propertyService
     *
     * @var undefined
     */
    private $propertyService;

     /**
     * __construct
     *
     * @param PropertyService $propertyService
     * @return void
     */
    public function __construct(
        PropertyService $propertyService
    ) {
        $this->propertyService = $propertyService;
    }

    /**
     * index
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $properties = $this->propertyService->list($request->all());
        return PropertyResource::collection($properties);
    }
}
