<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/property/list', 'PropertyController@index');
