
# PropSearch

This is a Laravel Vue/Vuex test project

![Demo](fe-interaction.gif)

## Environment

- Yarn 1.19.2 
- Node 12.16.2
- PHP 7.4.2

## Back End Set Up

- Rename .env.example file to .env inside your project root and fill the database information.
- Run `composer install` inside project directory
- Run `php artisan key:generate`
- Run `php artisan migrate`
- Run `php artisan db:seed` to apply seeders
- Run `php artisan serve` - this starts the backend, at this point you should be able to access the api route at [localurl](http://127.0.0.1:8000/api/property/list)
- Run phpunit feature tests to ensure everything is working `php vendor/phpunit/phpunit/phpunit`

## Front end set up

- Run `yarn` to install all node modules
- Run `yarn run` to spin up the front end
- Front end tests are available under `yarn test`
- At this point site should be up at [localurl](http://127.0.0.1:8000/)

## Known bugs

- Sometimes after running `yarn test` there is an error `Unable to locate Mix file: /css/app.css` present on the front end browser window - restarting `yarn run` console tab fixes this problem.

## Todo

- Backend tests coverage/unit and more feature
- Frontend vuex actions test
