import expect from 'expect';
import store from '../../../resources/js/store';
import mutations from '../../../resources/js/store/mutations';

const { RESET_FILTERS, START_LOADING, STOP_LOADING, FETCH_ERROR } = mutations;

describe('mutations', () => {

  it('START_LOADING', () => {
    const state = { loading: false };
    START_LOADING(state);
    expect(state.loading).toBeTruthy();
  });

  it('STOP_LOADING', () => {
    const state = { loading: true };
    STOP_LOADING(state);
    expect(state.loading).toBeFalsy();
  });

  it('FETCH_ERROR', () => {
    const state = { error: true };
    FETCH_ERROR(state, {});
    expect(state.error).toEqual('Error');
  });

  it('RESET_FILTERS', () => {
    const state = {
        filters: {
            name: 'Test',
            beds: 5,
            baths: 5,
            storey: 5,
            garage: 5,
            price: [5,100],
        }
    };

    RESET_FILTERS(state);
        expect(state.filters).toEqual({
            name: '',
            beds: '',
            baths: '',
            storey: '',
            garage: '',
            price: [200000,600000],
        });
    });
});
