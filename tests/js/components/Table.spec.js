import { createLocalVue, shallowMount } from '@vue/test-utils';
import expect from 'expect';
import Vuex from 'vuex';
import ElementUI from 'element-ui';

import store from '../../../resources/js/store';
import Table from '../../../resources/js/components/Table';

describe('Table', () => {
    const localVue = createLocalVue();
    localVue.use(Vuex);
    localVue.use(ElementUI);

    it('Renders element ui table columns component', () => {
        const wrapper = shallowMount(Table, {
            store,
            localVue
        });
        expect(wrapper.html()).toContain('el-table-column');
    });
});
