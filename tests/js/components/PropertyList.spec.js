import { createLocalVue, shallowMount } from '@vue/test-utils';
import expect from 'expect';
import Vuex from 'vuex';
import ElementUI from 'element-ui';

import store from '../../../resources/js/store';
import PropertyList from '../../../resources/js/components/PropertyList';

describe('PropertyList', () => {
    const localVue = createLocalVue();
    localVue.use(Vuex);
    localVue.use(ElementUI);

    it('Renders property list component', () => {
        const wrapper = shallowMount(PropertyList, {
            store,
            localVue
        });
        expect(wrapper.html()).toContain('<h1>Property List</h1>');
    });
});
