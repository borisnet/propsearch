import { createLocalVue, shallowMount } from '@vue/test-utils';
import expect from 'expect';
import Vuex from 'vuex';
import ElementUI from 'element-ui';

import store from '../../../resources/js/store';
import Filters from '../../../resources/js/components/Filters';

describe('Filters', () => {
    const localVue = createLocalVue();
    localVue.use(Vuex);
    localVue.use(ElementUI);

    it('Renders element ui price slider component', () => {
        const wrapper = shallowMount(Filters, {
            store,
            localVue
        });
        expect(wrapper.html()).toContain('el-slider-stub');
    });
});
