<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PropertyListTest extends TestCase
{

    /**
     * testListEndpoint
     *
     * @return void
     */
    public function testListEndpoint()
    {
        $response = $this->get('/api/property/list');
        $response->assertStatus(200);
    }

    /**
     * testPropertyListJsonStructure
     *
     * @return void
     */
    public function testPropertyListJsonStructure()
    {
        $response = $this->get('/api/property/list');
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'price',
                    'beds',
                    'baths',
                    'storey',
                    'garage',
                    'created_at',
                    'updated_at',
                ],
            ],
        ]);
    }

    /**
     * testPropertyListFilterName
     *
     * @return void
     */
    public function testPropertyListFilterName()
    {
        $response = $this->get('/api/property/list?name=Xavier');
        $response->assertStatus(200);
        $response->assertJsonCount(1, 'data');
    }

    /**
     * testPropertyListFilterPrice
     *
     * @return void
     */
    public function testPropertyListFilterPrice()
    {
        $response = $this->get('/api/property/list?price=550000,600000');
        $response->assertStatus(200);
        $response->assertJsonCount(1, 'data');
    }

    /**
     * testPropertyListFilterBeds
     *
     * @return void
     */
    public function testPropertyListFilterBeds()
    {
        $response = $this->get('/api/property/list?beds=3');
        $response->assertStatus(200);
        $response->assertJsonCount(2, 'data');
    }

    /**
     * testPropertyListFilterBaths
     *
     * @return void
     */
    public function testPropertyListFilterBaths()
    {
        $response = $this->get('/api/property/list?baths=3');
        $response->assertStatus(200);
        $response->assertJsonCount(3, 'data');
    }

    /**
     * testPropertyListFilterStorey
     *
     * @return void
     */
    public function testPropertyListFilterStorey()
    {
        $response = $this->get('/api/property/list?storey=2');
        $response->assertStatus(200);
        $response->assertJsonCount(6, 'data');
    }

    /**
     * testPropertyListFilterGarage
     *
     * @return void
     */
    public function testPropertyListFilterGarage()
    {
        $response = $this->get('/api/property/list?garage=3');
        $response->assertStatus(200);
        $response->assertJsonCount(1, 'data');
    }

    /**
     * testFailPropertyListFilterNameNoMatch
     *
     * @return void
     */
    public function testFailPropertyListFilterNameNoMatch()
    {
        $response = $this->get('/api/property/list?name=NoNameTestTitle');
        $response->assertStatus(200);
        $response->assertJsonCount(0, 'data');
    }
}
